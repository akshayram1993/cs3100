#ifndef COUNTER_H
#define COUNTER_H

#include "person.h"
#include <stdio.h>

class counter
{
	friend class queueManager;
public:
	// Constructor
	counter() {
		timeUntilFree = 0;
		numOfGoods = 0;
		servicingCustomer = NULL;
	}

	
	
private:
	int numOfGoods;		// Denotes the number of goods left in the counter.
	int timeUntilFree;  // Denotes the time after which the counter becomes free.
	person* servicingCustomer;	// Gives a pointer to a person who is being serviced by the counter.

	// All the methods defined here are internal methods. A user cannot interact with an object from the counter class.

	int getNumOfGoods(); 										// Returns the number of goods left in the counter
	
	void setNumOfGoods(int newNumOfGoods);		  				// This method is used at the time of initialization and simulation.   
																// This sets the number of goods present in the counter.							
	
	int getTimeUntilFree();										// This function returns the time after which the counter becomes free.
	
	void setTimeUntilFree(int newTimeUntilFree);				// This function is used at the time when a customer is allocated to the 
																// counter and at the end of each unit of time.
	
	void setServicingCustomer(person* newServicingCustomer);	// Sets the value of the servicing customer field

	person* getServicingCustomer();								// Returns the details about the person who is serviced by the counter.		
	
	bool isFree() ;												// Returns true if the counter is not servicing any request.	

	bool canService();											// Returns true if the counter has any goods left.
	
	void serviceRequest(person* requestor);						// Allocates a particular person to the counter.
};

#endif