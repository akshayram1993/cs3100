#include "exitQueue.h"
#include "person.h"

#include <stdio.h>
#include <stdlib.h>

// Print the contents of the exit queue.
void exitQueue::printQueue() {
	int numOfPersons = exit.size();
	int i = 0;
	if(numOfPersons == 0) {
		printf("The queue is empty\n");
	}
	else {
		printf("The persons standing in the queue are\n");
		for(i = 0; i < numOfPersons; i++) {
			person* temp = exit.front();
			printf("Queue Position = %d,Person number = %d, Checking time left = %d\n",i,temp->getPersonNumber(),temp->getServicedTime());
			exit.pop();
			exit.push(temp);
		}
	}
	
}
// Update the checking time left for the person at the head of the exit queue.
void exitQueue::update() {
	if(exit.size() > 0) {
		person* temp = exit.front();
		temp -> setServicedTime(temp -> getServicedTime() - 1);
		if(temp -> getServicedTime() == 0) {
			exit.pop();
			temp -> setStatus(3);
		}
	}
}