#include "display.h"

#include <stdio.h>
#include <iostream>

void display::Display(queueManager* Q, entryQueue* ent, exitQueue* ext, person* target) {
	
	if(displayCode == 0) {
		Q->printCounterStatus();
	}

	else if(displayCode == 1) {
		ent->printQueue();
	}

	else if(displayCode == 2) {
		ext->printQueue();
	}

	else if(displayCode == 3) {
		target->printPersonStatus();
	}
}

void display::setDisplayCode(int newDisplayCode) {
	displayCode = newDisplayCode;
}

int display::getDisplayCode() {
	return displayCode;
}