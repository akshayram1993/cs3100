#include "simulate.h"
#include "counter.h"
#include "person.h"
#include "exitQueue.h"
#include "entryQueue.h"

#include <stdio.h>
#include <iostream>

void simulate::queueSimulator(queueManager* Q, entryQueue* ent, exitQueue* ext, person* target, display* d) {
	int time = 0;		// Global notion of time
	bool flag = true;			// Checks whether the simulation is over or not	
	int oldTime = -1;	// Variable for formatting printing

	while(Q->isOver(ent) == false) {
		if(ent->entry.size() > 0) {
			person* head = ent->entry.front();
			int counterNumber = Q->canServiceRequest(); // Get the counter number that can service a request. It is -1 if none of the 
														// counters can service the request.


			while(counterNumber == -1) {
				ext->update();	// Update the exit queue.
				Q->update(ext);	// Update the counter status
				time++;			// Increment time
				oldTime = time;
				printf("At time %d\n",time);
				d->Display(Q,ent,ext,target);	// Call to the display class
				
				if(Q->isOver(ent)) {			// If the simulation is over break from the while loop
					flag = false;
					break;
				}
				counterNumber = Q->canServiceRequest();
			}

			if(flag == false) {
				break;
			}

			if(oldTime == -1 && d->getDisplayCode() == 1) {
				printf("At time %d\n",time);
				d->Display(Q,ent,ext,target);
			}
			// Add the person at the head of the queue to the counter that can service his request.
			head->setStatus(1);					// Update his status
			head->setAllotedCounter(counterNumber);	// Set the alloted counter number
			Q->allocateToCounter(counterNumber,head);	// Allocate the person to the specific counter  number.
			ent->entry.pop();					// Remove the person from the entry queue	
			if(oldTime != time || d->getDisplayCode() != 2) {
				printf("At time %d\n",time);
				d->Display(Q,ent,ext,target);
				oldTime = time;
			} 

		}
		else {
			ext->update();	// If the entry queue is empty wait till all the counters have serviced their requests.
			Q->update(ext);
			time++;
			printf("At time %d\n",time);
			d->Display(Q,ent,ext,target);
			
		}
	}
	// If all the counters have serviced their requests, then wait till every person in the exit queue exits.
	while(ext->exit.size() > 0) {
		ext->update();
		time++;
		printf("At time %d\n",time);
		d->Display(Q,ent,ext,target);
	}
	printf("The simulation is over\n");
}