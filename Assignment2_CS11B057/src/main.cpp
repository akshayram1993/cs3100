#include <stdio.h>
#include <stdlib.h>

#include "person.h"
#include "entryQueue.h"
#include "exitQueue.h"
#include "counter.h"
#include "queueManager.h"
#include "display.h"
#include "simulate.h"

int main() {
	
	int numOfPersons;	// Number of persons in the entry queue.
	int numOfCounters;	// Number of counters.
	entryQueue ent;		// Entry queue.
	exitQueue ext;		// Exit queue
	int displayCode;	// Display code
	person* target = NULL;	// person to be tracked
	int personNumber;		// Person id
	simulate S;				// simulation class

	int* initial;
	int i = 0;
	FILE* f = fopen("input.txt","r");

	printf("Enter the number of persons\n");
	scanf("%d",&numOfPersons);

	ent.initialize(numOfPersons);	// Initialize the queue.

	printf("Enter the number of counters present\n");
	scanf("%d",&numOfCounters);

	queueManager Q(numOfCounters);	

	initial = new int[numOfCounters];

	// Obtaining the number of goods in each counter.
	for(i = 0; i < numOfCounters; i++) {
		fscanf(f,"%d",&initial[i]);
	}

	Q.initializeCounters(initial);	// Initialize the counters

	
	printf("Enter the display code\n0:Counters view\n1:Entry queue view\n2:Exit queue view\n3:Person's view\n");
	scanf("%d",&displayCode);
	display d;
	d.setDisplayCode(displayCode);

	if(displayCode == 3) {
		printf("Enter the person number to be tracked\n");
		scanf("%d",&personNumber);
		target = ent.getTargetPerson(personNumber);
	}

	S.queueSimulator(&Q,&ent,&ext,target,&d);	// Instantiate the simulation.
}

