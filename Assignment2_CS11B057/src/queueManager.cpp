#include "queueManager.h"
#include "entryQueue.h"
#include "exitQueue.h"

// Function to initialize the counters
void queueManager::initializeCounters(int* initial) {
	int i = 0;
	for(i = 0; i < numOfCounters; i++) {
		counters[i].setNumOfGoods(initial[i]);
	}
}

// At the end of a time unit update the status of counters and add serviced persons to the exit queue.
void queueManager::update(exitQueue* ext) {
	int i = 0;
	for(i = 0; i < numOfCounters; i++) {
		if(! counters[i].isFree()) {
			counters[i].setNumOfGoods(counters[i].getNumOfGoods() - 1);	// Update the number of goods in the counter
			counters[i].setTimeUntilFree(counters[i].getTimeUntilFree() - 1);
			// If the customer has been serviced add him to the exit queue.
			if(counters[i].getTimeUntilFree() == 0) {
				person* exitingPerson = counters[i].getServicingCustomer();
				counters[i].setServicingCustomer(NULL);
				exitingPerson->setStatus(2);
				ext->exit.push(exitingPerson);
			}
		}
	}
}
// Checks the counter which is capable of servicing a customer's request.
int queueManager::canServiceRequest() {
	int i = 0;
	for(i = 0; i < numOfCounters; i++) {
		if(counters[i].canService() && counters[i].isFree()) {
			return i;
		}
	}
	return -1;
}

// Allocate a specific customer to a counter
void queueManager::allocateToCounter(int counterNumber, person* requestor) {
	counters[counterNumber].serviceRequest(requestor);
}

// Checks if the simulation is over.
bool queueManager::isOver(entryQueue* ent) {
	int i = 0;
	if(ent->entry.size() > 0) {
		for(i = 0; i < numOfCounters; i++) {
			if(counters[i].canService()) {
				return false;
			}
		}	
	}
	else {
		for(i = 0; i < numOfCounters; i++) {
			if(!counters[i].isFree()) {
				return false;
			}
		}
	}
	return true;
}

// Prints the status of each counter.
void queueManager::printCounterStatus() {
	int i = 0;
	for(i = 0; i < numOfCounters; i++) {
		if(counters[i].getNumOfGoods() == 0) {
			printf("Counter number: %d is closed\n",i);
		}
		else if(counters[i].getTimeUntilFree() == 0) {
			printf("Counter number: %d has %d goods and is free\n",i,counters[i].getNumOfGoods());
		}
		else
			printf("Counter number: %d has %d goods and will be free in the next %d units of time and is servincing person number %d\n",i,counters[i].getNumOfGoods(),counters[i].getTimeUntilFree(),(counters[i].getServicingCustomer())->getPersonNumber());
	}
}

// returns the customer who exits from a counter
person* queueManager::getExitingCustomer(int counterNumber) {
		person* exitingCustomer = counters[counterNumber].getServicingCustomer();
		return exitingCustomer;
}