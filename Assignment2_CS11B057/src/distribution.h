#ifndef DISTRIBUTION_H
#define DISTRIBUTION_H

#include <time.h>
#include <stdlib.h>

class distribution
{
public:
	// Interface function. User can add any probability distribution and can describe the sample function which samples the given 
	// probability distribution.
	int sample();
};

#endif