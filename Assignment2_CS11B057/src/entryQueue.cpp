#include "entryQueue.h"
#include "person.h"
#include "distribution.h"

#include "stdio.h"

// Initializes the entry queue with request times from a probability distribution specified in the distribution.cpp file.
void entryQueue::initialize(int numOfPersons) {
	int i = 0;
	distribution dist;

	for(i = 0; i < numOfPersons; i++) {
		person* temp = new person;
		int requestTime;

		requestTime = dist.sample();

		temp->setRequestTime(requestTime);
		temp->setPersonNumber(i);

		entry.push(temp);
	}
}

// Print the contents of the queue
void entryQueue::printQueue() {
	int numOfPersons = entry.size();
	int i = 0;
	if(numOfPersons == 0) {
		printf("The queue is empty\n");
	}
	else {
		printf("The persons standing in the queue are\n");
		for(i = 0; i < numOfPersons; i++) {
			person* temp = entry.front();
			printf("Queue Position = %d,Person number = %d, Request time = %d\n",i,temp->getPersonNumber(),temp->getRequestTime());
			entry.pop();
			entry.push(temp);
		}	
	}
}

person* entryQueue::getTargetPerson(int personNumber) {
	int n = entry.size();
	int i = 0;
	person* target = NULL;

	for(i = 0; i < n; i++) {
		person* temp = entry.front();
		if(temp->getPersonNumber() == personNumber) {
			target = temp;
		}
		entry.pop();
		entry.push(temp);
	}
	return target;
}