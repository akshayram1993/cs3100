#include "distribution.h"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// Fill this function with the sampler code for any probabilty distribution.
int distribution::sample() {
	static FILE* f = fopen("sample.txt","r");
	int n ;
	fscanf(f,"%d",&n);
	return n;
}