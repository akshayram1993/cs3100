#ifndef ENTRYQUEUE_H
#define ENTRYQUEUE_H

#include <queue>
#include <iostream>
#include "person.h"

class entryQueue
{
	friend class display;
public:

	// Public field. Data structure which holds the persons standing in the entry queue.
	std::queue<person*> entry;
	
	// Interface method. It initializes the entry queue by sampling from a probabilty distribution.
	void initialize(int numOfPersons);

	// This returns the pointer to the person who is to be tracked.
	person* getTargetPerson(int personNumber);

private:
	// Internal method used by the display class.
	void printQueue();
};

#endif