#ifndef EXITQUEUE_H
#define EXITQUEUE_H

#include <queue>
#include <iostream>
#include "person.h"

class exitQueue
{
	friend class simulate;
	friend class display;

public:
	// Data structure which holds the persons who have been serviced and have moved on to the exiting queue.
	std::queue<person*> exit;

private:
	// Internal method used by the display class.
	void printQueue();
	// Internal method. Updates the time at which the person at the head of the exit queue leaves the exit queue. 
	// Used by the simulate class.
	void update();

};
#endif