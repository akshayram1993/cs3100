#include "person.h"

#include <stdio.h>

// Getters and setters definitions

int person::getRequestTime() {
	return requestTime;
}
void person::setRequestTime(int newRequestTime) {
	requestTime = newRequestTime;
}
int person::getServicedTime() {
	return servicedTime;
} 
void person::setServicedTime(int newServicedTime) {
	servicedTime = newServicedTime;
}
void person::setPersonNumber(int newPersonNumber) {
	personNumber = newPersonNumber;
}
int person::getPersonNumber() {
	return personNumber;
}
int person::getStatus() {
	return status;
}
void person::setStatus(int newStatus) {
	status = newStatus;
}
int person::getAllotedCounter() {
	return allotedCounter;
}
void person::setAllotedCounter(int newAllotedCounter) {
	allotedCounter = newAllotedCounter;
}
void person::printPersonStatus() {
	if(status == 0) {
		printf("Person number %d is standing in the entry queue\n",personNumber);
	}

	else if(status == 1) {
		printf("Person number %d is standing in counter number %d\n",personNumber,allotedCounter);
	}

	else if(status == 2) {
		printf("Person number %d is standing in the exit queue\n",personNumber);
	}

	else {
		printf("Person has left the exit queue\n");
	}
}