/*****************************************************************************************************
* This is the queue simulator class. It contains one public method queueSimulator which is instantiated
* with objects from queueManager, entryQueue, exitQueue, person, and display class respectively.
******************************************************************************************************/

#ifndef SIMULATE_H
#define SIMULATE_H

#include "counter.h"
#include "person.h"
#include "exitQueue.h"
#include "entryQueue.h"
#include "queueManager.h"
#include "display.h"
#include <stdio.h>
#include <iostream>

class simulate
{
public:
	// Interface method. A user who wants to simulate a queue can call this method after initializing the arguments.
	// The arguments this function takes are:
	// queueManager Q: Has the info about the counters
	// entryQueue ent: Has the information about the request times of persons
	// exitQueue ext: Has information about serviced customers.
	// person target: Gives the information about the person who needs to be tracked.
	// display d: Has the information about from whose point of view the simulation should be displayed. 
	void queueSimulator(queueManager* Q, entryQueue* ent, exitQueue* ext, person* target, display* d);
};

#endif