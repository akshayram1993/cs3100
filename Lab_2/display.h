#ifndef DISPLAY_H
#define DISPLAY_H

#include "person.h"
#include "queueManager.h"
#include "entryQueue.h"
#include "exitQueue.h"

class display
{
	friend class simulate;
public:
	
	// Interface functions. User can set the display code and can also view the display code.
	void setDisplayCode(int newDisplayCode);
	int getDisplayCode();

private:
	int displayCode;
	// Internal function which is used by the simulate class.
	void Display(queueManager* Q, entryQueue* ent, exitQueue* ext, person* target);	
};

#endif