#ifndef QUEUEMANAGER_H
#define QUEUEMANAGER_H

#include "counter.h"
#include "person.h"
#include "exitQueue.h"
#include "entryQueue.h"
#include <stdio.h>
#include <iostream>

class queueManager
{
	friend class simulate;
	friend class display;

public:
	queueManager(int n) {
		numOfCounters = n;
		counters = new counter [n];
	}

	// Interface method used to initialize the counters with certain number of goods. 
	void initializeCounters(int* initial);
	
private:
	int numOfCounters;
	counter* counters; 

	void update(exitQueue* ext); // Updates the status of counters after unit time.
	int canServiceRequest();  // Returns the counter number that can service this particular request at the head of the queue
	void allocateToCounter(int counterNumber,person* requestor); // Allocates the requestor to a counter.
	bool isOver(entryQueue* ent);		// Checks whether the simulation is over		
	void printCounterStatus();			// Prints the status of counters.
	person* getExitingCustomer(int counterNumber); 
};

#endif