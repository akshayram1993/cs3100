#ifndef PERSON_H
#define PERSON_H
class person
{
	friend class entryQueue;
	friend class exitQueue;
	friend class simulate;
	friend class counter;
	friend class display;
	friend class queueManager;

public:
	person() {
		requestTime = 0;
		servicedTime = 0;
		personNumber = 0;
		status = 0;
	}
	~person();
	

private:
	int requestTime;
	int servicedTime;
	int personNumber;
	int status;
	int allotedCounter;

	// Internal methods that a user cannot access. These methods are accessed by the classes which are declared as friend classes 
	// to this class.

	// The following two functions gets and sets the customer request time. 
	int getRequestTime();
	void setRequestTime(int newRequestTime);

	// The following two functions gets and sets the time a customer spends at a counter.
	int getServicedTime();
	void setServicedTime(int newServicedTime);

	// The following two functions gets and sets the person number.
	void setPersonNumber(int newPersonNumber);
	int getPersonNumber();

	// The following two functions gets and sets the customer status.
	int getStatus();
	void setStatus(int newStatus);

	// The following function prints the customer status.
	void printPersonStatus(); 

	// The following two functions gets and sets the counter number to which a customer is alloted.
	int getAllotedCounter();
	void setAllotedCounter(int newAllotedCounter);
};

#endif