#include "simulate.h"
#include "counter.h"
#include "person.h"
#include "exitQueue.h"
#include "entryQueue.h"

#include <stdio.h>
#include <iostream>

void simulate::queueSimulator(queueManager* Q, entryQueue* ent, exitQueue* ext, person* target, display* d) {
	int time = 0;
	bool flag;
	int oldTime = -1;

	while(Q->isOver(ent) == false) {
		if(ent->entry.size() > 0) {
			person* head = ent->entry.front();
			int counterNumber = Q->canServiceRequest();

			while(counterNumber == -1) {
				ext->update();
				Q->update(ext);
				time++;
				oldTime = time;
				printf("At time %d\n",time);
				d->Display(Q,ent,ext,target);
				
				if(Q->isOver(ent)) {
					flag = false;
					break;
				}
				counterNumber = Q->canServiceRequest();
			}

			if(oldTime == -1 && d->getDisplayCode() == 1) {
				printf("At time %d\n",time);
				d->Display(Q,ent,ext,target);
			}
			head->setStatus(1);
			head->setAllotedCounter(counterNumber);
			Q->allocateToCounter(counterNumber,head);	
			ent->entry.pop();
			if(oldTime != time || d->getDisplayCode() != 2) {
				printf("At time %d\n",time);
				d->Display(Q,ent,ext,target);
				oldTime = time;
			} 

		}
		else {
			ext->update();
			Q->update(ext);
			time++;
			printf("At time %d\n",time);
			d->Display(Q,ent,ext,target);
			
		}
	}
	while(ext->exit.size() > 0) {
		ext->update();
		time++;
		printf("At time %d\n",time);
		d->Display(Q,ent,ext,target);
	}
	printf("The simulation is over\n");
}