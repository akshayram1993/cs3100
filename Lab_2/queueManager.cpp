#include "queueManager.h"
#include "entryQueue.h"
#include "exitQueue.h"

void queueManager::initializeCounters(int* initial) {
	int i = 0;
	for(i = 0; i < numOfCounters; i++) {
		counters[i].setNumOfGoods(initial[i]);
	}
}

void queueManager::update(exitQueue* ext) {
	int i = 0;
	for(i = 0; i < numOfCounters; i++) {
		if(! counters[i].isFree()) {
			counters[i].setNumOfGoods(counters[i].getNumOfGoods() - 1);
			counters[i].setTimeUntilFree(counters[i].getTimeUntilFree() - 1);
			if(counters[i].getTimeUntilFree() == 0) {
				person* exitingPerson = counters[i].getServicingCustomer();
				counters[i].setServicingCustomer(NULL);
				exitingPerson->setStatus(2);
				ext->exit.push(exitingPerson);
			}
		}
	}
}

int queueManager::canServiceRequest() {
	int i = 0;
	for(i = 0; i < numOfCounters; i++) {
		if(counters[i].canService() && counters[i].isFree()) {
			return i;
		}
	}
	return -1;
}

void queueManager::allocateToCounter(int counterNumber, person* requestor) {
	counters[counterNumber].serviceRequest(requestor);
}

bool queueManager::isOver(entryQueue* ent) {
	int i = 0;
	if(ent->entry.size() > 0) {
		for(i = 0; i < numOfCounters; i++) {
			if(counters[i].canService()) {
				return false;
			}
		}	
	}
	else {
		for(i = 0; i < numOfCounters; i++) {
			if(!counters[i].isFree()) {
				return false;
			}
		}
	}
	return true;
}

void queueManager::printCounterStatus() {
	int i = 0;
	for(i = 0; i < numOfCounters; i++) {
		if(counters[i].getNumOfGoods() == 0) {
			printf("Counter number: %d is closed\n",i);
		}
		else if(counters[i].getTimeUntilFree() == 0) {
			printf("Counter number: %d has %d goods and is free\n",i,counters[i].getNumOfGoods());
		}
		else
			printf("Counter number: %d has %d goods and will be free in the next %d units of time\n",i,counters[i].getNumOfGoods(),counters[i].getTimeUntilFree());
	}
}


person* queueManager::getExitingCustomer(int counterNumber) {
		person* exitingCustomer = counters[counterNumber].getServicingCustomer();
		return exitingCustomer;
}