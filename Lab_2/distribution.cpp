#include "distribution.h"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int distribution::sample() {
	static FILE* f = fopen("sample.txt","r");
	int n ;
	fscanf(f,"%d",&n);
	return n;
}