#include <stdio.h>
#include <stdlib.h>

#include "person.h"
#include "entryQueue.h"
#include "exitQueue.h"
#include "counter.h"
#include "queueManager.h"
#include "display.h"
#include "simulate.h"

int main() {
	
	int numOfPersons;
	int numOfCounters;
	entryQueue ent;
	exitQueue ext;
	int displayCode;
	person* target = NULL;
	int personNumber;
	simulate S;

	int* initial;
	int i = 0;
	FILE* f = fopen("input.txt","r");

	printf("Enter the number of persons\n");
	scanf("%d",&numOfPersons);

	ent.initialize(numOfPersons);

	printf("Enter the number of counters present\n");
	scanf("%d",&numOfCounters);

	queueManager Q(numOfCounters);

	initial = new int[numOfCounters];

	for(i = 0; i < numOfCounters; i++) {
		fscanf(f,"%d",&initial[i]);
	}

	Q.initializeCounters(initial);

	
	printf("Enter the display code\n0:Counters view\n1:Entry queue view\n2:Exit queue view\n3:Person's view\n");
	scanf("%d",&displayCode);
	display d;
	d.setDisplayCode(displayCode);

	if(displayCode == 3) {
		printf("Enter the person number to be tracked\n");
		scanf("%d",&personNumber);
		target = ent.getTargetPerson(personNumber);
	}

	S.queueSimulator(&Q,&ent,&ext,target,&d);
}

