/* 
 * File:   miniGrid.h
 * Author: akshayaram
 *
 * Created on August 3, 2013, 5:38 PM
 */

#include "cell.h"

#ifndef MINIGRID_H
#define	MINIGRID_H

class miniGrid {
public:
    cell startCell;
    cell endCell;
    int gridNum;
    int count[10];
    miniGrid() {
        gridNum = 0;
        int j = 0;
        for(j = 1; j<=9 ;j++) {
            count[j] = 0;
        }
    }
    int* countWithinGrid(cell grid[10][10]) {
        int i,j;
        int *total;
        total = new int[10];
        for(i = 0;i < 10; i++) {
            total[i] = 0;
        }
        for(i = startCell.getX(); i <= endCell.getX(); i++) {
            for(j = startCell.getY() ; j <= endCell.getY() ; j++) {
                total[(grid[i][j]).getNum()]++; 
            }
        }
        
        return total;
    }
    
    cell findCell(cell grid[10][10], int number) {
        int i,j;
        for(i = startCell.getX(); i <= endCell.getX(); i++) {
            for(j = startCell.getY() ; j <= endCell.getY() ; j++) {
                if(grid[i][j].getNum() == number) {
                    return grid[i][j];
                } 
            }
        }
        
    }
};

#endif	/* MINIGRID_H */

