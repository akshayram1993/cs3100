/* 
 * File:   cell.h
 * Author: akshayaram
 *
 * Created on August 3, 2013, 5:33 PM
 */

#ifndef CELL_H
#define	CELL_H

class cell {
public:
     cell() {
        x = 0;
        y = 0;
        number = 0;
    }
    int getX() {
        return x;
    }
    void setX(int newX) {
        x = newX;
    }
    int getY() {
        return y;
    }
    void setY(int newY) {
        y = newY;
    }
    int getNum() {
        return number;
    }
    void setNum(int newNum) {
        number = newNum;
    }
    
private:
    int number;
    int x;
    int y;

};

#endif	/* CELL_H */

