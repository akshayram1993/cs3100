#include <cstdlib>
#include <stdio.h>
#include <iostream>


#include "cell.h"
#include "miniGrid.h"


cell mainGrid[10][10];
cell solvedGrid[10][10];

/*
Subroutine cellSwap contains the code for giving the sequence of steps for swapping the entries of any 2 cells in the sudoku
without changing the configuration of any other cells.

More details about the routine can be found in Documentations.pdf.
*/
void cellSwap(cell A, cell B) {
    
    if(A.getX() < B.getX()) {
        cell temp = A;
        A = B;
        B = temp;
    }
    
    if(A.getX() != B.getX()) {

        if(A.getY() <= B.getY()) {
            if(B.getY() % 3 == 1 || B.getY() % 3 == 2 ) {
                printf("Rotate row number %d clock-wise by %d units\n",A.getX(),B.getY() - A.getY() + 1);
                printf("Rotate column number %d clock-wise by %d units\n",B.getY()+1,A.getX() - B.getX());
                printf("Swap the entries in the cells (%d,%d) and (%d, %d)\n",B.getX(),B.getY(),B.getX(),B.getY()+1);
                printf("Rotate column number %d anti clock-wise by %d units\n",B.getY()+1,A.getX() - B.getX());
                printf("Rotate row number %d anti clock-wise by %d units\n",A.getX(),B.getY() - A.getY() + 1);
            }
            else {
                if(B.getY() != A.getY() + 1) {
                    printf("Rotate row number %d clock-wise by %d units\n",A.getX(),B.getY() - A.getY() - 1);
                }
                printf("Rotate column number %d clock-wise by %d units\n",B.getY()-1,A.getX() - B.getX());
                printf("Swap the entries in the cells (%d,%d) and (%d, %d)\n",B.getX(),B.getY(),B.getX(),B.getY()-1);
                printf("Rotate column number %d anti clock-wise by %d units\n",B.getY()-1,A.getX() - B.getX());
                printf("Rotate row number %d anti clock-wise by %d units\n",A.getX(),B.getY() - A.getY() - 1);
            }
        }
        else {
            if(B.getY() % 3 == 1 || B.getY() % 3 == 2) {
                if(A.getY() != B.getY() + 1) {
                    printf("Rotate row number %d anti clock-wise by %d units\n",A.getX(),A.getY() - B.getY() - 1);
                }
                printf("Rotate column number %d clock-wise by %d units\n",B.getY()+1,A.getX() - B.getX());
                printf("Swap the entries in the cells (%d,%d) and (%d, %d)\n",B.getX(),B.getY(),B.getX(),B.getY()+1);
                printf("Rotate column number %d anti clock-wise by %d units\n",B.getY()+1,A.getX() - B.getX());
                printf("Rotate row number %d clock-wise by %d units\n",A.getX(),A.getY() - B.getY() - 1);
            }
            else {
                printf("Rotate row number %d anti clock-wise by %d units\n",A.getX(),A.getY() - B.getY() + 1);
                printf("Rotate column number %d clock-wise by %d units\n",B.getY()-1,A.getX() - B.getX());
                printf("Swap the entries in the cells (%d,%d) and (%d, %d)\n",B.getX(),B.getY(),B.getX(),B.getY()-1);
                printf("Rotate column number %d anti clock-wise by %d units\n",B.getY()-1,A.getX() - B.getX());
                printf("Rotate row number %d clock-wise by %d units\n",A.getX(),A.getY() - B.getY() + 1);
            }

        }
    }
    else {
        if(A.getY() > B.getY()) {
            cell temp = A;
            A = B;
            B = temp;
        }
        
        if(B.getX() % 3 == 1 || B.getX() % 3 == 2 ) {
            printf("Rotate column number %d anti clock-wise by 1 units\n",A.getY());
            printf("Rotate row number %d clock-wise by %d units\n",A.getX()+1,B.getY() - A.getY());
            printf("Swap the entries in the cells in (%d,%d) and (%d, %d)\n",B.getX(),B.getY(),B.getX() + 1,B.getY());
            printf("Rotate row number %d anti clock-wise by %d units\n",A.getX()+1,B.getY() - A.getY());
            printf("Rotate column number %d clock-wise by 1 units\n",A.getY());
        }
        else {
            printf("Rotate column number %d clock-wise by 1 units\n",A.getY());
            printf("Rotate row number %d clock-wise by %d units\n",A.getX()+1,B.getY() - A.getY());
            printf("Swap the entries in the cells (%d,%d) and (%d, %d)\n",B.getX(),B.getY(),B.getX() - 1,B.getY());
            printf("Rotate row number %d anti clock-wise by %d units\n",A.getX()+1,B.getY() - A.getY());
            printf("Rotate column number %d anti clock-wise by 1 units\n",A.getY());
        }

    }
}
/*
Subroutine correctTheGrid contains the code for getting the sequence of moves for obtaining a solved SudoKu

Proof of correctness for the routine is contained in the Documentation.pdf file.
*/

void correctTheGrid(miniGrid mg[10], int* countForGrid[10]) {
    int i,j,k,l;
    
    for(i = 1; i < 10; i++) {
        int deficitNum;
        cell excessCell1,excessCell2;
        for(k = 1; k < 10; k++) {
            if(countForGrid[i][k] == 0) {
                deficitNum = k;
                for(j = i+1; j < 10; j++) {
                    if(countForGrid[j][deficitNum] > 1) {
                        excessCell2 = mg[j].findCell(mainGrid,deficitNum);
                        break;
                    }
                }
                for(l = 1; l < 10 ; l++) {
                    if(countForGrid[i][l] > 1) {
                        excessCell1 = mg[i].findCell(mainGrid,l);
                        break;
                    }
                }
                cellSwap(excessCell1,excessCell2);
                printf("\n");
                int temp = mainGrid[excessCell1.getX()][excessCell1.getY()].getNum();
                mainGrid[excessCell1.getX()][excessCell1.getY()].setNum(mainGrid[excessCell2.getX()][excessCell2.getY()].getNum());
                mainGrid[excessCell2.getX()][excessCell2.getY()].setNum(temp);
                countForGrid[i] = mg[i].countWithinGrid(mainGrid);
                countForGrid[j] = mg[j].countWithinGrid(mainGrid);
         
            }
        }
        
    }
}

void printGrid(cell grid[10][10]) {
    int i,j;
    for(i=1;i<10;i++) {
        for(j=1;j<10;j++) {
            printf("%d ",grid[i][j].getNum());
        }
        printf("\n");
    }
}

using namespace std;



int main(int argc, char** argv) {
    
    miniGrid mg[10];
    int* countForGrid[10];
    FILE* f;
    FILE* solved;
    f = fopen("input.txt","r");
    solved = fopen("solved.txt","r");
    
    int i = 0, j = 0;
    
    for(i = 1; i < 10; i++) {
        for(j = 1; j < 10; j++) {
            int newNum; 
            fscanf(f,"%d",&newNum);
            mainGrid[i][j].setNum(newNum);
            mainGrid[i][j].setX(i);
            mainGrid[i][j].setY(j);
        }
    }
    
    for(i = 1; i < 10; i++) {
        for(j = 1; j < 10; j++) {
            int newNum; 
            fscanf(solved,"%d",&newNum);
            solvedGrid[i][j].setNum(newNum);
            solvedGrid[i][j].setX(i);
            solvedGrid[i][j].setY(j);
        }
    }
    
    for(i = 1; i < 10; i++) {
        if(i % 3 != 0) {
            mg[i].startCell = mainGrid[(i/3)*3 + 1][1 + (i%3 -1)*3];
            mg[i].endCell = mainGrid[(i/3)*3 + 3][3 + (i%3 - 1)* 3];
            mg[i].gridNum = i;
            countForGrid[i] = mg[i].countWithinGrid(mainGrid);
        }
        else {
            mg[i].startCell = mainGrid[((i-1)/3)*3 + 1][7];
            mg[i].endCell = mainGrid[((i-1)/3)*3 + 3][9];
            mg[i].gridNum = i;
            countForGrid[i] = mg[i].countWithinGrid(mainGrid);
        }
                  
    }
    /*
    Code for checking the necessary conditions.
    */
    int flag[10] ;
    for(i = 1; i < 10; i++) {
        flag[i] = 0;
    }
    
    for(i = 1; i < 10; i++) {
        for(j = 1; j < 10; j++) {
            flag [i] += countForGrid[j][i]; 
        }
    }
    int solvable = 1;
    for(i = 1; i < 10; i++) {
        if(flag[i] != 9) {
            solvable = 0;
            break;
        }
    }
    if(solvable == 1) {
        printf("The SudoKu is solvable and the sequence of valid moves for obtaining a solution is given by \n\n");
        correctTheGrid(mg,countForGrid);
        printf("\n");
        printf("The SudoKu configuration got after these sequence of moves is \n\n");
        printGrid(mainGrid);
        printf("\n");
        printf("Now permute each mini grid so that we obtain the following valid SudoKu\n\n");
        printGrid(solvedGrid);
    }
    else {
        printf("The SudoKu is unsolvable ie., there is no sequence of valid moves that can generate a valid Sudoku\n");
    }
    fclose(f);
    fclose(solved);
    return 0;
}

