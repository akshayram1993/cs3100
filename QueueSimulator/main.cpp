#include <iostream>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#include "entryQueue.h"
#include "exitQueue.h"
#include "person.h"
#include "counter.h"
#include "queueManager.h"
#include "distribution.h"


void queueSimulator(queueManager Q,entryQueue* ent, exitQueue* ext) {
	int i = 9;
	bool flag = true;
	person* exitingPerson;
	int time = 0;
	int personNumber = 0;
	person* lastAllocatedCustomer = NULL;
	while(Q.isOver() == false) {
		
		int counterNumber = Q.canServiceRequest();
		while(counterNumber == -1) {
			printf("At time %d\n",time);
			printf("The person at the head of the queue (person number = %d) waits\n",personNumber);
			printf("At the end of time %d, the status of the counters is\n",time);
			Q.update(ext);
			
			Q.printCounterStatus();
			counterNumber = Q.canServiceRequest();
			time++;
			if(Q.isOver()) {
				flag = false;
				break;
			}
			person* temp = new person;
			temp->setRequestTime(i*3 + 2);
			ent->entry.push(temp);
			i++;
		}
		
		
		
		if(flag == false) {
			break;
		}
		printf("At time %d\n",time);
		printf("The person at the head of the queue (person number = %d) is alloted to %d\n",personNumber,counterNumber);

		
		person* temp = ent->entry.front();
		temp->setPersonNumber(personNumber);
		Q.allocateToCounter(counterNumber,temp);
		ent->entry.pop();
		personNumber++;
		//Q.printCounterStatus();
	}
	
	printf("All the counters are closed and the simulation is over\n");	
}


int main() {
	int i = 0;
	entryQueue ent;
	exitQueue ext;
	distribution prob;

	for(i = 0;i<5;i++) {
		person* temp = new person;
		int requestTime = prob.sample();
		temp->setRequestTime(requestTime);
		ent.entry.push(temp);
	}
	/*for(i=0;i<5;i++) {
		person* temp =  ent.entry.front();
		std::cout<<temp->getRequestTime()<<"\n";
		ent.entry.pop();
		ext.exit.push(temp);
	}
	for(i=0;i<5;i++) {
		person* temp =  ext.exit.front();
		std::cout<<temp->getRequestTime()<<"\n";
		ext.exit.pop();
	}*/
	queueManager Q = queueManager(5);
	int init[5];
	for(i = 0; i < 5;i++) {
		init[i] = i*i + 5;
	}
	bool flag = true;
	Q.initializeCounters(init);
	printf("The initial status of the counters is \n");
	Q.printCounterStatus();
	//queueSimulator(Q,&ent,&ext);

		
}


