#ifndef COUNTER_H
#define COUNTER_H

#include "person.h"
#include <stdio.h>
class counter
{
public:
	counter() {
		timeUntilFree = 0;
		numOfGoods = 0;
		servicingCustomer = NULL;
	}
	int getNumOfGoods() {
		return numOfGoods;
	}
	void setNumOfGoods(int newNumOfGoods) {
		numOfGoods = newNumOfGoods;
	}
	int getTimeUntilFree() {
		return timeUntilFree;
	}
	void setTimeUntilFree(int newTimeUntilFree) {
		timeUntilFree = newTimeUntilFree;
	}
	void setServicingCustomer(person* newServicingCustomer) {
		servicingCustomer = newServicingCustomer;
	}
	person* getServicingCustomer() {
		return servicingCustomer;
	}
	bool isFree() ;
	bool canService();
	void serviceRequest(person* requestor);

private:
	int numOfGoods;
	int timeUntilFree;
	person* servicingCustomer;
};
#endif