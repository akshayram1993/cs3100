#ifndef PERSON_H
#define PERSON_H
class person
{
public:
	person() {
		requestTime = 0;
		servicedTime = 0;
	}
	~person();
	int getRequestTime() {
		return requestTime;
	}
	void setRequestTime(int newRequestTime) {
		requestTime = newRequestTime;
	}
	int getServicedTime() {
		return servicedTime;
	} 
	void setServicedTime(int newServicedTime) {
		servicedTime = newServicedTime;
	}
	void setPersonNumber(int newPersonNumber) {
		personNumber = newPersonNumber;
	}
	int getPersonNumber() {
		return personNumber;
	}

private:
	int requestTime;
	int servicedTime;
	int personNumber;
};

#endif