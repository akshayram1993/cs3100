#include "counter.h"
#include "person.h"
#include <iostream>
#include <stdio.h>

bool counter::isFree() {
	if(timeUntilFree > 0) {
		return false;
	}
	return true;
}

bool counter::canService() {
	if(numOfGoods > 0) {
		return true;
	}
	return false;
}

void counter::serviceRequest(person* requestor) {
	
	if(requestor->getRequestTime() <= numOfGoods) {
		timeUntilFree = requestor->getRequestTime();
		requestor->setServicedTime(timeUntilFree);
		
	}
	else {
		timeUntilFree = numOfGoods;
		requestor->setServicedTime(numOfGoods);	
	}

	setServicingCustomer(requestor);
}