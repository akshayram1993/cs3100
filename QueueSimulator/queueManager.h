#ifndef QUEUEMANAGER_H
#define QUEUEMANAGER_H

#include "counter.h"
#include "person.h"
#include "exitQueue.h"
#include "entryQueue.h"
#include <stdio.h>
#include <iostream>

class queueManager
{
public:
	queueManager(int n) {
		numOfCounters = n;
		counters = new counter [n];
	}

	void initializeCounters(int* initial);
	void update(exitQueue* ext);
	int canServiceRequest();  // Returns the counter number that can service this particular request at the head of the queue
	void allocateToCounter(int counterNumber,person* requestor);
	bool isOver();
	void printCounterStatus();
	person* getExitingCustomer(int counterNumber); 
	void simulateQueue();

private:
	int numOfCounters;
	counter* counters; 
};

#endif