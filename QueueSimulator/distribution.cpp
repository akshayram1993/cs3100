#include "distribution.h"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int distribution::sample() {
	srand(time(NULL));
	int n = rand()%15 + 1;
	return n;
}