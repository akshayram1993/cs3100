
solve( [[X11,X12,X13,X14],[X21,X22,X23,X24],
 [X31,X32,X33,X34],[X41,X42,X43,X44]]) :-
 %rows
 permutation([X11,X12,X13,X14],[1,2,3,4]),
 permutation([X21,X22,X23,X24],[1,2,3,4]),
 permutation([X31,X32,X33,X34],[1,2,3,4]),
 permutation([X41,X42,X43,X44],[1,2,3,4]),
 %cols
 permutation([X11,X21,X31,X41],[1,2,3,4]),
 permutation([X12,X22,X32,X42],[1,2,3,4]),
 permutation([X13,X23,X33,X43],[1,2,3,4]),
 permutation([X14,X24,X34,X44],[1,2,3,4]),
 %boxes
 permutation([X11,X12,X21,X22],[1,2,3,4]),
 permutation([X13,X14,X23,X24],[1,2,3,4]),
 permutation([X31,X32,X41,X42],[1,2,3,4]),
 permutation([X33,X34,X43,X44],[1,2,3,4]).

% Need to check the necessary condition to avoid infinite computation

% already solved sudoku

sudoku([[X11,X12,X13,X14],[X21,X22,X23,X24],
 [X31,X32,X33,X34],[X41,X42,X43,X44]]) :-

 rotate([[X11,X12,X13,X14],[X21,X22,X23,X24],
 [X31,X32,X33,X34],[X41,X42,X43,X44]],[0,0,0,0,0,0,0,0]) .

% Row rotations

firstRowRotate([[X11,X12,X13,X14],[X21,X22,X23,X24],
 [X31,X32,X33,X34],[X41,X42,X43,X44]], [R1,R2,R3,R4,C1,C2,C3,C4]) :-

R1<4, Board = [[X14,X11,X12,X13],[X21,X22,X23,X24],
 [X31,X32,X33,X34],[X41,X42,X43,X44]] , X is R1 + 1, Counter = [X,R2,R3,R4,C1,C2,C3,C4], rotate(Board,Counter).

secondRowRotate([[X11,X12,X13,X14],[X21,X22,X23,X24],
 [X31,X32,X33,X34],[X41,X42,X43,X44]], [R1,R2,R3,R4,C1,C2,C3,C4]) :-

R2<4, Board = [[X11,X12,X13,X14],[X24,X21,X22,X23],
 [X31,X32,X33,X34],[X41,X42,X43,X44]] , X is R2 + 1, Counter = [R1,X,R3,R4,C1,C2,C3,C4], rotate(Board,Counter).

thirdRowRotate([[X11,X12,X13,X14],[X21,X22,X23,X24],
 [X31,X32,X33,X34],[X41,X42,X43,X44]], [R1,R2,R3,R4,C1,C2,C3,C4]) :-

R3<4, Board = [[X11,X12,X13,X14],[X21,X22,X23,X24],
 [X34,X31,X32,X33],[X41,X42,X43,X44]] , X is R3 + 1, Counter = [R1,R2,X,R4,C1,C2,C3,C4], rotate(Board,Counter).


fourthRowRotate([[X11,X12,X13,X14],[X21,X22,X23,X24],
 [X31,X32,X33,X34],[X41,X42,X43,X44]], [R1,R2,R3,R4,C1,C2,C3,C4]) :-

R4<4, Board = [[X11,X12,X13,X14],[X21,X22,X23,X24],
 [X31,X32,X33,X34],[X44,X41,X42,X43]] , X is R4 + 1, Counter = [R1,R2,R3,X,C1,C2,C3,C4], rotate(Board,Counter).


 firstColumnRotate([[X11,X12,X13,X14],[X21,X22,X23,X24],
 [X31,X32,X33,X34],[X41,X42,X43,X44]], [R1,R2,R3,R4,C1,C2,C3,C4]) :-

C1<4, Board = [[X41,X12,X13,X14],[X11,X22,X23,X24],
 [X21,X32,X33,X34],[X31,X42,X43,X44]] , X is C1 + 1, Counter = [R1,R2,R3,R4,X,C2,C3,C4], rotate(Board,Counter).


 secondColumnRotate([[X11,X12,X13,X14],[X21,X22,X23,X24],
 [X31,X32,X33,X34],[X41,X42,X43,X44]], [R1,R2,R3,R4,C1,C2,C3,C4]) :-

C2<4, Board = [[X11,X42,X13,X14],[X21,X12,X23,X24],
 [X31,X22,X33,X34],[X41,X32,X43,X44]] , X is C2 + 1, Counter = [R1,R2,R3,R4,C1,X,C3,C4], rotate(Board,Counter).


 thirdColumnRotate([[X11,X12,X13,X14],[X21,X22,X23,X24],
 [X31,X32,X33,X34],[X41,X42,X43,X44]], [R1,R2,R3,R4,C1,C2,C3,C4]) :-

C3<4, Board = [[X11,X12,X43,X14],[X21,X22,X13,X24],
 [X31,X32,X23,X34],[X41,X42,X33,X44]] , X is C3 + 1, Counter = [R1,R2,R3,R4,C1,C2,X,C4], rotate(Board,Counter).


 fourthColumnRotate([[X11,X12,X13,X14],[X21,X22,X23,X24],
 [X31,X32,X33,X34],[X41,X42,X43,X44]], [R1,R2,R3,R4,C1,C2,C3,C4]) :-

C4<4, Board = [[X11,X12,X13,X44],[X21,X22,X23,X14],
 [X31,X32,X33,X24],[X41,X42,X43,X34]] , X is C4 + 1, Counter = [R1,R2,R3,R4,C1,C2,C3,X], rotate(Board,Counter).


rotate([[X11,X12,X13,X14],[X21,X22,X23,X24],
 [X31,X32,X33,X34],[X41,X42,X43,X44]],[R1,R2,R3,R4,C1,C2,C3,C4]) :-

solve([[X11,X12,X13,X14],[X21,X22,X23,X24],
 [X31,X32,X33,X34],[X41,X42,X43,X44]]) .

rotate([[X11,X12,X13,X14],[X21,X22,X23,X24],
 [X31,X32,X33,X34],[X41,X42,X43,X44]],[R1,R2,R3,R4,C1,C2,C3,C4]) :- 

	firstRowRotate([[X11,X12,X13,X14],[X21,X22,X23,X24],
 [X31,X32,X33,X34],[X41,X42,X43,X44]],[R1,R2,R3,R4,C1,C2,C3,C4]) .

rotate([[X11,X12,X13,X14],[X21,X22,X23,X24],
 [X31,X32,X33,X34],[X41,X42,X43,X44]],[R1,R2,R3,R4,C1,C2,C3,C4]) :-
	
	secondRowRotate([[X11,X12,X13,X14],[X21,X22,X23,X24],
 [X31,X32,X33,X34],[X41,X42,X43,X44]],[R1,R2,R3,R4,C1,C2,C3,C4]).
	
rotate([[X11,X12,X13,X14],[X21,X22,X23,X24],
 [X31,X32,X33,X34],[X41,X42,X43,X44]],[R1,R2,R3,R4,C1,C2,C3,C4]) :-

	thirdRowRotate([[X11,X12,X13,X14],[X21,X22,X23,X24],
 [X31,X32,X33,X34],[X41,X42,X43,X44]],[R1,R2,R3,R4,C1,C2,C3,C4]) .

rotate([[X11,X12,X13,X14],[X21,X22,X23,X24],
 [X31,X32,X33,X34],[X41,X42,X43,X44]],[R1,R2,R3,R4,C1,C2,C3,C4]) :-

 	fourthRowRotate([[X11,X12,X13,X14],[X21,X22,X23,X24],
 [X31,X32,X33,X34],[X41,X42,X43,X44]],[R1,R2,R3,R4,C1,C2,C3,C4]) .

 rotate([[X11,X12,X13,X14],[X21,X22,X23,X24],
 [X31,X32,X33,X34],[X41,X42,X43,X44]],[R1,R2,R3,R4,C1,C2,C3,C4]) :-

 	firstColumnRotate([[X11,X12,X13,X14],[X21,X22,X23,X24],
 [X31,X32,X33,X34],[X41,X42,X43,X44]],[R1,R2,R3,R4,C1,C2,C3,C4]) .

 rotate([[X11,X12,X13,X14],[X21,X22,X23,X24],
 [X31,X32,X33,X34],[X41,X42,X43,X44]],[R1,R2,R3,R4,C1,C2,C3,C4]) :-

 	secondColumnRotate([[X11,X12,X13,X14],[X21,X22,X23,X24],
 [X31,X32,X33,X34],[X41,X42,X43,X44]],[R1,R2,R3,R4,C1,C2,C3,C4]) .

 rotate([[X11,X12,X13,X14],[X21,X22,X23,X24],
 [X31,X32,X33,X34],[X41,X42,X43,X44]],[R1,R2,R3,R4,C1,C2,C3,C4]) :-

 	thirdColumnRotate([[X11,X12,X13,X14],[X21,X22,X23,X24],
 [X31,X32,X33,X34],[X41,X42,X43,X44]],[R1,R2,R3,R4,C1,C2,C3,C4]) .

 rotate([[X11,X12,X13,X14],[X21,X22,X23,X24],
 [X31,X32,X33,X34],[X41,X42,X43,X44]],[R1,R2,R3,R4,C1,C2,C3,C4]) :-

 	fourthColumnRotate([[X11,X12,X13,X14],[X21,X22,X23,X24],
 [X31,X32,X33,X34],[X41,X42,X43,X44]],[R1,R2,R3,R4,C1,C2,C3,C4]) .



