fun length([]) = 0
|   length(a::x) = 1 + length(x);

(* This function takes a polynomial a(x) and an integer r as input and returns the coefficient of x^r as the result *)

fun getCoefficient(a,r) = if(r = length(a) - 1) then hd(a)
						  else getCoefficient(tl(a),r);

(* This function takes two polynomials as input and returns the sum of them as the result *)

fun sum((a: real list), (b : real list)) = if(length(a) = 0) then b
										   else if(length(b) = 0) then a
										   else if(length(a) = length(b)) then (hd(a) + hd(b)) :: sum(tl(a), tl(b))
										   else if(length(a) > length(b)) then (hd(a)) :: sum(tl(a),b)
										   else hd(b):: sum(a,tl(b));


(* This function takes a polynomial p(x) as input and returns -p(x) *)

fun negate((a:real list)) = if(length(a) = 0) then a
							else (0.0 - hd(a)):: negate(tl(a));

fun removeLeadingZeroes(a: real list) = if(length(a) = 0) then a
										else if(hd(a) >= 0.0 andalso hd(a) <= 0.0) then removeLeadingZeroes(tl(a))
										else a;


(* This function takes two polynomials p(x) and q(x) as input and returns p(x) - q(x) *)

fun diff((a: real list), (b : real list)) = removeLeadingZeroes(sum(a,negate(b)));


(* This function takes a polynomial and pads 0 at the end until the polynomial has appropriate degree *)

fun padZeroes((a:real list), r) = if(length(a) >= r) then a
								  else padZeroes(a @ [0.0], r);


(* This function takes p(x) and q(x) as arguments and returns p(x)q(x) *)

fun product((a: real list), (b: real list)) = if(null(a)) then a
											  else if(null(b)) then b
											  else 
											  		let 
											  			val len_a = length(a);
											  			val len_b = length(b);
											  			val x = hd(a);
											  		in
											  			sum(padZeroes((map (fn y => x*y) b),len_b + len_a -1), product(tl(a),b))
											  		end;

(* This function takes a polynomial as input and returns the integral of the polynomial as the result. *)

fun integrate((a:real list)) = if(null(a)) then a
							   else
							   		sum(padZeroes([hd(a) / (Real.fromInt(length(a)))], length(a) + 1), integrate(tl(a))); 


(* This function takes two polynomials a(x) and b(x) and returns the quotient when a(x) is divided by b(x) *)

fun getQuotient((a: real list), (b: real list)) = if(length(a) < length(b)) then []
												  else if(length(a) = length(b)) then [hd(a)/hd(b)]
												  else 
												  		let	
												  			val len_a = length(a);
											  				val len_b = length(b);
											  				val x = hd(a) / hd(b);
											  				val intermediateProduct = padZeroes((map (fn y => x*y) (tl(b): real list)),len_a - 1)
											  				val newDividend = diff(tl(a),intermediateProduct)
											  			in
											  				(* sum(padZeroes([x],len_a-len_b + 1), getQuotient(newDividend,b))*)
											  				[x] @ getQuotient(newDividend,b)
											  			end;

(* This function takes two polynomials a(x) and b(x) and returns the remainder when a(x) is divided by b(x) *)

fun getRemainder((a: real list), (b: real list)) = let	
														val quotient = getQuotient(a,b);
														val intermediateProduct = product(quotient,b);
													in 
														diff(a, intermediateProduct)
													end;



fun pow(a:real,b:int,n:int) = if(b = 0) then 1.0
							 else if(n = b) then a
							 else
							 	a*pow(a,b,n+1);

fun evaluate(f:real list, x: real)  = 
									if(length(f) = 0) then 0.0
									else 
										let 
											val a = hd(f);
											val b = pow(x,length(f)-1,1);
										in
											a*b + evaluate(tl(f),x)
										end;


fun getFactorsOfInteger(i : int, state: int) = if(state > abs(i)) then []
											   else if(i mod state = 0) then [state] @ getFactorsOfInteger(i,state+1)
											   else getFactorsOfInteger(i,state+1);

fun getCandidateRationals(f:int list, g:int list)  = if(length(f) = 0) then []
													else
													let 
														val headOfF = Real.fromInt(hd(f));
														val MappedList = map (fn x => Real.fromInt(x) / headOfF)g;
													in
														MappedList @ getCandidateRationals(tl(f),g)
													end;

fun getLastCoefficient(f:int list) = if(length(f) = 1) then hd(f)
									 else getLastCoefficient(tl(f));


fun checkIfZero(a: real list) = if(length(a) = 0) then true
								else if(hd(a) >= 0.0 andalso hd(a) <= 0.0) then checkIfZero(tl(a))
								else false;

fun getRationalRoots(f:int list, roots: real list) = if(length(roots) = 0) then []
												 else
												 let 
												 val root = hd(roots);
												 val factor1 = [1.0,0.0-root];
												 val factor2 = [1.0,root];
												 val rem1 = getRemainder(map (fn x => Real.fromInt(x))f,factor1);
												 val rem2 = getRemainder(map (fn x => Real.fromInt(x))f,factor2);
												 in 
												 	if(checkIfZero(rem1) andalso checkIfZero(rem2)) then [root,0.0 - root] @ getRationalRoots(f,tl(roots))
												 	else if(checkIfZero(rem1)) then [root] @ getRationalRoots(f,tl(roots))
												 	else if(checkIfZero(rem2)) then [0.0 - root] @ getRationalRoots(f,tl(roots))
 												 	else getRationalRoots(f,tl(roots))
												 end;

fun getRationalFactors(f: int list) = let
										val factorsOfa_n = getFactorsOfInteger(hd(f),1);
										val a_0 = getLastCoefficient(f);
										val factorsOfa_0 = getFactorsOfInteger(a_0,1);
										val candidateRoots = getCandidateRationals(factorsOfa_n,factorsOfa_0);
										val roots = getRationalRoots(f,candidateRoots);
									  in
									  	if(a_0 = 0) then roots@[0.0]
									  	else
									  		roots
									  end;

fun getFactors(r:real list) = if(length(r) = 0) then []
							  else [[1.0,0.0 - hd(r)]] @ getFactors(tl(r)) ;

fun productOfFactors(r: real list list) = if(length(r) = 0) then [1.0]
										  else
										  		product(productOfFactors(tl(r)),hd(r));

fun factorize(f:int list) = let 
								val roots = getRationalFactors(f);
								val prod = productOfFactors(getFactors(roots));
								val quotient = getQuotient(map (fn x => Real.fromInt(x))f,prod);

							in
								getFactors(roots)@[quotient]
							end;


fun gcd(a: real list, b: real list) = if(checkIfZero(b)) then a
									  else 
									  	let
									  		val remainder = getRemainder(a,b);
									  	in
									  		gcd(b,removeLeadingZeroes(remainder))
									  	end; 

fun first(x,y) = x;

fun second(x,y) = y;

fun extendedEuclid(a: real list, b: real list) = if(checkIfZero(b)) then ([1.0],[0.0])
												 else 
												 	let
												 		val rem = getRemainder(a,b);
												 		val X = extendedEuclid(b,removeLeadingZeroes(rem));
												 		val quotient = getQuotient(a,b);
												 		val firstTerm = second(X);
												 		val secondTerm = diff(first(X),product(second(X),quotient));
												 	in
												 		(removeLeadingZeroes(firstTerm),removeLeadingZeroes(secondTerm) )
												 	end;



fun decompose(p1: real list, q1: real list, p2: real list, q2: real list) = let 
																				val g = gcd(q1,q2);
																				val X = extendedEuclid(q1,q2);
																				val P = product(p1,p2);
																				val A1 = product(P,first(X));
																				val A2 = product(P,second(X));
																				val B1 = product(q2,g);
																				val B2 = product(q1,g);
																			in

																				(A1,B1,A2,B2)
																			end;