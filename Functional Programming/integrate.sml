fun integrate((a:real list)) = if(null(a)) then a
							   else 
							   		(hd(a) / Real.fromInt(length(a))) :: integrate(tl(a));